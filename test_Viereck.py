from unittest import TestCase
from Viereck import Viereck
import math

class TestViereck(TestCase):
    def setUp(self):
        self.quadratMitEinerSeitenlaenge = Viereck(1)
        self.quadratMitZweiSeitenLaengen = Viereck(1, 1)
        self.rechteck = Viereck(3, 4)
        pass

    def tearDown(self):
        pass

    def test_info(self):
        self.assertEqual(self.quadratMitEinerSeitenlaenge.info, 'Quadrat')
        self.assertEqual(self.quadratMitZweiSeitenLaengen.info, 'Quadrat')
        self.assertEqual(self.rechteck.info, 'Rechteck')

    def test_berechne_flaeche(self):
        self.assertEqual(self.quadratMitEinerSeitenlaenge.berechneFlaeche(), 1)
        self.assertEqual(self.quadratMitZweiSeitenLaengen.berechneFlaeche(), 1)
        self.assertEqual(self.rechteck.berechneFlaeche(), 12)

    def test_berechne_diagonale(self):
        self.assertEqual(self.quadratMitEinerSeitenlaenge.berechneDiagonale(), math.sqrt(2))
        self.assertEqual(self.quadratMitZweiSeitenLaengen.berechneDiagonale(), math.sqrt(2))
        self.assertEqual(self.rechteck.berechneDiagonale(), 5)
