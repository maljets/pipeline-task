# This is a sample Python script.

# Press Umschalt+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import sys
import math

class Viereck:
    ersteSeite: float = None
    zweiteSeite: float = None
    info = None
    flaeche: float = None
    diagonale: float = None


    def __init__(self, ersteSeite: float, zweiteSeite: float =None):
        self.ersteSeite = ersteSeite
        self.zweiteSeite = zweiteSeite

        if ersteSeite is not None:
            if zweiteSeite is not None and ersteSeite != zweiteSeite:
                self.info = 'Rechteck'
            else:
                self.info = 'Quadrat'

    def berechneFlaeche(self):
        if self.info == 'Rechteck':
            self.flaeche = self.ersteSeite * self.zweiteSeite
        if self.info == 'Quadrat':
            self.flaeche = self.ersteSeite ** 2
        return self.flaeche

    def berechneDiagonale(self):
        if self.info == 'Rechteck':
            self.diagonale = math.sqrt((self.ersteSeite ** 2) + (self.zweiteSeite ** 2))
        if self.info == 'Quadrat':
            self.diagonale = math.sqrt(2 * (self.ersteSeite ** 2))
        return self.diagonale


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Mindestens eine Seitenlänge angeben!')
    else:
        ersteSeite = float(sys.argv[1])
        if len(sys.argv) > 2:
            zweiteSeite = float(sys.argv[2])
        else:
            zweiteSeite = None
        viereck = Viereck(ersteSeite, zweiteSeite)
        flaeche = viereck.berechneFlaeche()
        diagonale = viereck.berechneDiagonale()
        print('{0}: Fläche: {1}, Diagonale: {2}'.format(viereck.info, flaeche, diagonale))

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
